# CI/CD Library

**📚 Documentation:** https://makerstreet-public.gitlab.io/ci-cd-library

---

This is a repository of CI/CD jobs and rules that our projects at Makerstreet can take advantage of. 

It consists of a set of common jobs and rules (inside `/common`) and platform-specific jobs, rules, and even entire pipelines.

> ⚠️ Notice that this repository is mirrored [here](https://gitlab.com/makerstreet-public/ci-cd-library).
> 
> Contributions MUST be done in the private repository, but you MUST use the public repository
> in your CI/CD pipelines.


- [Usage](#usage)
- [Contributing](#contributing)
  - [Documentation](#documentation)


## Usage

Import the necessary libraries into your `.gitlab-ci.yml` file like so:

```yml
include:
  - project: "makerstreet-public/ci-cd-library"
    ref: main
    file: "common/jobs.yml"
  - project: "makerstreet-public/ci-cd-library"
    ref: main
    file: "common/rules.yml"
  - project: "makerstreet-public/ci-cd-library"
    ref: main
    file: "python/jobs.yml"
```

You can specify a [version tag](https://gitlab.com/makerstreet-public/ci-cd-library/-/tags) in the `ref` keyword, to ensure that your build won't break due to an update in this library.

Note that in order to import `common` jobs, you'll need a stage called `test`. This is a consequence of GitLab's AutoDevOps enforcing those stage names.


## Contributing

In this project we enforce [conventional commits](https://www.conventionalcommits.org/) guidelines for commit messages. The usage of [commitizen](https://commitizen-tools.github.io/commitizen/) is recommended, but not required.

Ensuring backwards compatibility is a **must** and breaking changes need to be very carefully considered before merging.

### Documentation

To view the documentation locally, install the `just` command runner:

```sh
brew install just
# or
snap install --edge --classic just
```

And run the following command:

```sh
just docs
```

This will start a local server that you can visit at http://127.0.0.1:8000.
