# Project Token

Some of the jobs defined in these templates make use of a project environment variable called `PROJECT_TOKEN` in order to grant the CI permissions to make additional changes to your repository, such as automatically bumping a version, generating a changelog, etc.

To set up this `PROJECT_TOKEN`:
1. Go to **Settings** > **Access Tokens** > **Project Access Tokens**.
1. In **Token Name** set: `project-token`.
1. Remove the Expiration date.
1. Select role **Developer**.
1. Select **only** *write_repository*.
1. Then press the button **Create project access token**.
1. Copy the contents of the token: they will only be displayed once.
  [![Gitlab-Project-Token](./images/gitlab-project-token.png)](./images/gitlab-project-token.png)
1. Go to **Settings** > **CI/CD** > **VARIABLES**.
1.  Press the button **Add variable**.
1.   Set `PROJECT_TOKEN` as **Key**, and the value of the token as **Value**.
1.   Make sure the checkboxe **Mask variable** is checked.    
  [![Gitlab-CI-Variable](./images/gitlab-ci-variable.png)](./images/gitlab-ci-variable.png)
1.   Finally, go to **Settings** > **Repository** > **Protected Branches**.
1.   Select **Maintainers** and **Developers** in the **Allowed to merge**.
1.   Select the bot user that starts with **project-token-** in the **Allow to push** dropdown.
  [![Gitlab-Branch-Permissions](./images/gitlab-repo-permissions.png)](./images/gitlab-repo-permissions.png)
