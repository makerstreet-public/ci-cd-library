# Common Jobs and Rules

## Rules

### .on-merge-request-or-version-bump

Runs on merge requests or on bump commits (those starting with `^bump:`) on the main branch.

### .on-version-bump

Runs on the main branch, only on bump commits (those starting with `^bump:`).

### .on-merge-request

Runs on merge requests.

### .on-main-and-not-version-bump

Runs on the main branch, but only when not bumping the version, that is, when the commit does not start with `^bump:`.

### .on-manual

Runs only when executed manually.

### .on-manual-allow-failure

Runs only when executed manually, allows for failure.

### .on-merge-request-and-main-always

Runs always on merge requests and the default branch.

### .on-merge-request-always

Runs always on merge requests.

### .on-main-on-success

Runs on the default branch only if previous jobs were successful.


## Jobs

### .do-bump-version

Bumps the version of the software using semantic versioning, using [`commitizen`](commitizen-tools.github.io/commitizen/). It requires a `PROJECT_TOKEN` variable to work, with at least `read/write` permissions to the repository, and with the ability to push to the `CI_DEFAULT_BRANCH`. It accepts a variable (`TAG_PREFIX`) to inject a prefix to the version tag (default value: `v`), this should match the prefix commitizen configuration you have set up in `tag_format`.

- Example:

```yaml
bump-version:
  stage: bump-version
  extends: 
    - .do-bump-version 
    - .on-main-and-not-version-bump
```

### .do-lint-commit

Ensures commits ahead of the `CI_DEFAULT_BRANCH` follow [conventional commit rules](https://www.conventionalcommits.org/en/v1.0.0/), using [`commitizen`](commitizen-tools.github.io/commitizen/). It requires a `PROJECT_TOKEN` variable to work, with at least `read` permissions to the repository.

- Example:

```yaml
lint-commit:
  extends: 
    - .do-lint-commit 
    - .on-merge-request
```

### .do-template-check

Checks if the current project is up to date with its corresponding [`cruft`](https://cruft.github.io/cruft/) template. Requires the variable `TEMPLATE_TOKEN` to be set, which by default is set to the `CI_JOB_TOKEN`. It is allowed to failed through `allow_failure: true`, but that can be overridden.

- Example:

```yaml
template-check:
  stage: test
  extends: 
    - .do-template-check 
    - .on-merge-request
```


### .do-docs

Publishes documentation in `docs` folder to GitLab pages. Needs to run in a job called `pages`. It supports optional plugins by setting the `PLUGINS` variable.

- Example:

```yaml
pages:
  stage: pages 
  extends: 
    - .do-docs
    - .on-version-bump
  variables:
    PLUGINS: 'mkdocs-awesome-pages-plugin'
```

It requires an mkdocs configuration file (called `mkdocs.yaml`, see [configuration docs](https://www.mkdocs.org/user-guide/configuration/)) at the root of the repository, like this one:

```yml
# Project information
site_name: YOUR_PROJECT_NAME
  YOUR_PROJECT_DESCRIPTION

# Repo info
repo_url: YOUR_PROJECT_REPO_URL

# Configuration
theme:
  name: material
  language: en

  # Don't include MkDocs' JavaScript
  include_search_page: false
  search_index_only: true

  features:
    - content.code.annotate
    - content.tooltips
    - navigation.indexes
    - navigation.sections
    - navigation.expand
    - navigation.top
    - navigation.tracking
    - search.highlight
    - search.share
    - search.suggest

  icon:
    repo: fontawesome/brands/gitlab

  palette:
    - scheme: default
      primary: indigo
      accent: indigo
      toggle:
        icon: material/toggle-switch
        name: Switch to dark mode
    - scheme: slate
      primary: indigo
      accent: indigo
      toggle:
        icon: material/toggle-switch-off-outline
        name: Switch to light mode

# Page tree - add the md files you want displayed here
nav:
  - Home:
      - Intro: index.md

plugins:
  - search
```


### .do-validate-docs

Validates that docs can be built and are correct, e.g.: they don't contain broken links, the mkdocs.yaml file is correct, etc. It supports optional plugins by setting the `PLUGINS` variable, like so:

```yaml
validate-docs:
  extends: .do-validate-docs
  variables:
    PLUGINS: 'mkdocs-awesome-pages-plugin'
```


### .do-determine-tag

Determines the current version tag of the project. Only works if the project uses [`commitizen`](commitizen-tools.github.io/commitizen/). Saves the version tag into a variable `TAG` that other jobs can use through `artifacts`. It accepts a variable (`TAG_PREFIX`) to inject a prefix to the version tag (default value: `v`).

- Example:

```yaml
determine-tag:
  stage: publish
  extends:
    - .do-determine-tag
    - .on-version-bump 
  variables:
    TAG_PREFIX: '' # remove `v` prefix
```


### .do-spell-check

Looks for spelling errors in files. By default it only checks markdown files, but this can be configured using the variable `FILE_PATTERN`. Uses [`codespell`](https://github.com/codespell-project/codespell), and its version can be selected through the variable `CODESPELL_VERSION`.

- Example:

```yaml
spell-check:
  stage: test
  extends: 
   - .do-spell-check
   - .on-merge-request
  variables:
     FILE_PATTERN: '**/*.rst **/*.md **/*.txt'
     CODESPELL_VERSION: '2.2.4'
```

### .do-dependency-security

Scans dependencies for vulnerabilities using Google's [`osv-scanner`](https://github.com/google/osv-scanner). Compatible with many lockfiles/languages; see [the docs](https://google.github.io/osv-scanner/usage/#specify-lockfiles). Accepts additional arguments for `osv-scanner` through the `ARGS` variable.

- Example:

```yaml
dependency-security:
  stage: test
  extends: 
    - .do-dependency-security 
    - .on-merge-request
  variables:
    ARGS: '--config pyproject.toml'
```

---


!!! Note

    The following jobs do not follow our naming standards. This is because they come from GitLab AutoDevOps. They also assume that they run in a stage called `test`, which you may need
    to set if you import this file. We ensure they never run (unless you extend from them) through a `when: never` rule.

### semgrep-sast

Static Application Security Testing compatible with many programming languages. See [the documentation](https://docs.gitlab.com/ee/user/application_security/sast/).

- Example:

```yaml
code-security:
  stage: test
  extends: 
    - semgrep-sast 
    - .on-merge-request-or-version-bump
```

### nodejs-scan-sast

Static Application Security Testing for nodejs projects, see the [original repository](https://github.com/ajinabraham/nodejsscan).

- Example:

```yaml
code-security:
  stage: test
  extends: 
    - nodejs-scan-sast
    - .on-merge-request-or-version-bump
```

### container_scanning

Scans Docker containers for vulnerabilities. See [the documentation](https://docs.gitlab.com/ee/user/application_security/container_scanning/). It can scan any Docker image by overriding the variable `CS_IMAGE` (default value `$CI_REGISTRY_IMAGE`).

- Example:

```yaml
container-security:
  stage: test
  extends: 
    - container_scanning 
    - .on-merge-request-or-version-bump
  variables:
    CS_IMAGE: $CI_REGISTRY_IMAGE/myimage:1.0.0
```


### secret_detection

Looks for leaked secrets in the repository using [`gitleaks`](https://docs.gitlab.com/ee/user/application_security/secret_detection/).

- Example:

```yaml
secret-detection:
  stage: test
  extends: 
    - secret_detection 
    - .on-merge-request-or-version-bump
```
