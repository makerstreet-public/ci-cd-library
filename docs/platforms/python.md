# Python 

These are the jobs and rules for python builds.


## Jobs

### .do-dependency-check

Checks that both pyproject.toml and poetry.lock are correct and in sync. Override the `image` if you are using a different python version in your project (default `python 3.11`). The version of poetry can be set with the variable `POETRY_VERSION`.

- Example:

```yaml
dependency-check:
  stage: test
  extends: 
    - .do-dependency-check 
    - .on-merge-request
```


### .do-changelog-from-latest-release

Creates an incremental changelog from the latest GitLab release, to the current change. Only works if the project uses [`commitizen`](commitizen-tools.github.io/commitizen/). The changelog is passed to other jobs through artifacts, in a file called `RELEASE_CHANGELOG.md`.

- Example:

```yaml
release-changelog:
  stage: publish
  extends:
    - .do-changelog-from-latest-release
    - .on-version-bump

release:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: deploy
  extends: .on-version-bump
  needs:
    - job: release-changelog
      artifacts: true
  script:
    - echo "Creating production release"
  release:
    name: 'Release'
    description: ./RELEASE_CHANGELOG.md
    tag_name: '0.0.0'
    ref: '$CI_COMMIT_SHORT_SHA'
  when: manual
```
