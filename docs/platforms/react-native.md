# React-native 

These are the jobs and rules for react-native builds.


## Jobs

### .do-lint

Runs linting on the project using the linting command provided by `yarn lint`

If your project uses private packages, you can provide the `$NPM_NAMESPACE` variable so that npm can lookup your private gitlab packages.
This variable can be set in this job or at the CI-file level if you have many js-jobs.

- Example:

```yaml
variables:
  NPM_NAMESPACE: '@your-npm-namespace'

lint:
  stage: test
  extends: 
    - .do-lint 
    - .on-merge-request
```


### .do-compile

Runs a compilation check on the project by compiling using the typescript compiler

If your project uses private packages, you can provide the `$NPM_NAMESPACE` variable so that npm can lookup your private gitlab packages.
This variable can be set in this job or at the CI-file level if you have many js-jobs.

- Example:

```yaml
variables:
  NPM_NAMESPACE: '@your-npm-namespace'

compile:
  stage: test
  extends: 
    - .do-compile 
    - .on-merge-request
```

### .do-test

Runs tests on the project using the linting command provided by `yarn test`

If your project uses private packages, you can provide the `$NPM_NAMESPACE` variable so that npm can lookup your private gitlab packages.
This variable can be set in this job or at the CI-file level if you have many js-jobs.

- Example:

```yaml
variables:
  NPM_NAMESPACE: '@your-npm-namespace'

compile:
  stage: test
  extends: 
    - .do-test
    - .on-merge-request
```

### .do-build

Defines a set of extendable build jobs for either **ios builds**, **android builds** or **androidbundle builds**. You can provide the `ENVIRONMENT` variable to each of these jobs to control the fastlane `.env.environment` to be used in these builds.

If your project uses private packages, you can provide the `$NPM_NAMESPACE` variable so that npm can lookup your private gitlab packages.
This variable can be set in this job or at the CI-file level if you have many js-jobs.

- Example:

```yaml
variables:
  NPM_NAMESPACE: '@your-npm-namespace'

ios-staging:
  stage: build
  extends: [.do-ios-build, .on-merge-request-or-protected]
  variables:
    ENVIRONMENT: staging

android-staging:
  stage: build
  extends: [.do-android-build, .on-merge-request-or-protected]
  variables:
    ENVIRONMENT: staging

ios-production:
  stage: build
  extends: [.do-ios-build, .on-main]
  variables:
    ENVIRONMENT: production

android-production:
  stage: build
  extends: [.do-android-build, .on-main]
  variables:
    ENVIRONMENT: production

androidbundle-production:
  stage: build
  extends: [.do-androidbundle-build, .on-main]
```

### .do-prepare-release
Tag the commit & generates metadata to be used when creating a release.
It will fetch the TAG from the commitizen configuration
& create a RELEASE_CHANGELOG with the changes of that tag

- Example:

```yaml
prepare-release:
  stage: release
  needs: []
  extends: [.do-prepare-release, .on-main]
```

### .do-release

Creates an incremental changelog from the latest GitLab release, to the current change. Only works if the project uses [`commitizen`](commitizen-tools.github.io/commitizen/). The changelog is passed to other jobs through artifacts, in a file called `RELEASE_CHANGELOG.md`.
Must depend on a previous job's artifacts `prepare-release`, which generates the metadata necessary for the release.

- Example:

```yaml
release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  extends: [.do-release, .on-main]
  when: manual
  needs:
    - job: prepare-release
      artifacts: true
```

## Pipelines

### Classic

A fully-fledged CI with:
- compilation test
- testing
- linting
- commit linting
- automatic versioning & changelog
- building for iOS & Android
- releasing with a release changelog

This is mostly created as an *example*, to show how the job templates created here can tie together in a full pipeline.
However it *should* also *just work* by importing it directly, like so:

```yaml
include:
  - project: "makerstreet-public/ci-cd-library"
    ref: main
    file: "react-native/pipelines/classic.yml"
```