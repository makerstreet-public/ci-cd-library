# Android

These are the jobs and rules for Android builds.

Uses `fastlane` jobs as defined in the `unifiedfastfile` repository.

Can be included through:

```yml
include:
  - project: "makerstreet-public/ci-cd-library"
    ref: main
    file: "android/jobs.yml"
```

## Variables

`JDK_VERSION`: default is `17.0.6-oracle` (uses SDKman)

## Mandatory jobs

### Lint

Description: performs static code analysis and linting
Runs: on MRs and default branch
Calls: `bundle exec fastlane lint` and `./gradlew lintDebug`

### MobSFAudit

Description: Performs [MobSFAudit](https://github.com/MobSF/Mobile-Security-Framework-MobSF) on the last build
Runs: on default branch

### pages

Description: generates code documentation for Gitlab pages
Runs: on default branch
Calls: .do-docs

## Base jobs

Some jobs make use of [dotenv](https://github.com/bkeepers/dotenv) to read variables for [fastlane](https://docs.fastlane.tools/best-practices/keys/). These should be defined inside the `fastlane` folder.

### .do-unit-tests

Runs: on MRs and default branch
Calls: `bundle exec fastlane test` to run unit tests on your project.
Variables: `ENVIRONMENT`, which fastlane environment

### .do-deployplus

Description: Base job for deploying to Deploy+

### .do-release

Description: Base job for release builds. Calls `bundle exec fastlane build` to build the project.
Build type: `release` is the default.
Variables: `ENVIRONMENT`, which fastlane environment

### .do-staging

Description: base job for staging builds
Runs: depends on project
Calls: `bundle exec fastlane build` to build the project.
Build type: `staging` is the default.
Variables: `ENVIRONMENT`, which fastlane environment

### .do-testbuild

Runs: on MRs and default branch
Calls :`bundle exec fastlane testbuild`
Variables: `ENVIRONMENT`, which fastlane environment

## Migration

From internal to public repo.

1. Use the follow include inside your `.gitlab-ci.yml`:

Full pipeline setup:

```yml
include:
  - project: "makerstreet-public/ci-cd-library"
    ref: main
    file: "android/pipeline.yml"
```

or only the jobs:

```yml
include:
  - project: "makerstreet-public/ci-cd-library"
    ref: main
    file: "android/jobs.yml"
```


2. Update `extends` references:

* `.unittests` to `.do-unit-tests`
* `.testbuild` to `.do-testbuild`
* `.release` to `.do-release`
* `.manual` to `.on-manual-allow-failure`
* `.staging` to `.do-staging`
* `.merge-request` to `.on-merge-request-manual`
* `.manual` to `.on-manual-allow-failure`
* `.main` to `.on-main-on-success`
* `.danger` to `.do-danger`

3. Remove any `before_script` block which overrides the `JAVA_HOME`.