# Javascript 

These are the jobs and rules for javascript builds.


## Jobs

### .do-publish-package

Publishes a javascript package to the project's registry.

If your project uses private packages, you can provide the `$NPM_NAMESPACE` variable so that npm can lookup your private gitlab packages.
This variable can be set in this job or at the CI-file level if you have many js-jobs.

- Example:

```yaml
variables:
  NPM_NAMESPACE: '@your-npm-namespace'

publish-package:
  stage: publish
  extends: 
    - .do-publish-package 
    - .on-version-bump
```
