# Development guidelines

At Elements we have created some guidelines to developing CI templates, loosely inspired by the [official gitlab templates guidelines](https://docs.gitlab.com/ee/development/cicd/templates.html)

There are basically 4 types of templates: **rules**, **base jobs**, **jobs** and **pipelines**

### Rules

Rules define _when_ to do something, providing a `rules` keyword. They should contain no actions or logic, and follow the naming convention `.on-<rule-name>`.

For example:

```yaml
.on-merge-request:
   rules:
     - if: $CI_MERGE_REQUEST_ID
```

### Base jobs

These jobs are reusable hidden jobs which can be reused across **Job** and **Pipeline** templates. They are not meant to be used directly by the end-user, but rather to have reusable logic that serves for other template jobs.

These hidden jobs **must** be prefixed by `.base-{BASE_JOB_NAME}`, for example

```yaml
.base-commitizen:
  tags: [docker]
  image:
    name: commitizen/commitizen:3.5.2
    entrypoint: ['']
  script:
    - git config user.email "${GITLAB_USER_EMAIL}"
    - git config user.name "${GITLAB_USER_LOGIN}"
    - git fetch --tags --force

```

### Jobs

Jobs define _what_ to do, providing a `script` keyword, and all other necessary keywords to ensure the `script` works. They all should follow the naming convention `.do-<job-name>`. The `before_script` is always left undefined, to allow those jobs that extend from these to define their own.

For example:

```yaml
.do-lint-commit:
  extends: [.base-commitizen]
  script:
    - !reference [.base-commitizen, script]
    - git fetch https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git $CI_MERGE_REQUEST_TARGET_BRANCH_NAME:$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - cz check --rev-range $CI_MERGE_REQUEST_TARGET_BRANCH_NAME..HEAD
  interruptible: true

```

### Pipelines

Pipeline templates define an entirely pre-defined template that can be `include`d within a project's CI.
It should be fully working and requires no configuration to get it up and running.

Note: The use-case of pipeline templates is still to be determined. It can be imported as-is, but in general projects will probably want to have flexibility and control over their CI, so it may not be of actual use to the end-user. Right now it is useful to show an **example** of a working template, and as a template that can be **validated** to check in our CI that all jobs work well together.
