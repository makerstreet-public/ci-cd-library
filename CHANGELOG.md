## v0.9.7 (2025-02-11)

### Technical

- bump poetry version

## v0.9.6 (2025-01-30)

### CI

- fix pinning template versions

## v0.9.5 (2024-10-25)

### Fix

- python version downgrade

## v0.9.4 (2024-10-10)

### Fix

- **deps**: pin osv-scanner

## v0.9.3 (2024-10-10)

### Technical

- **deps**: update all non-major dependencies

## v0.9.2 (2024-03-05)

### Technical

- **deps**: update all non-major dependencies

## v0.9.1 (2024-02-23)

### Technical

- **deps**: update all non-major dependencies

## v0.9.0 (2024-02-01)

### Feat

- react native jobs

## v0.8.4 (2024-01-22)

### Technical

- **deps**: update all non-major dependencies

## v0.8.3 (2024-01-10)

### Technical

- **deps**: update all non-major dependencies

## v0.8.2 (2023-12-20)

### Technical

- **deps**: update all non-major dependencies

## v0.8.1 (2023-11-14)

### CI

- use docker image gitlab-ci-validator instead of script

## v0.8.0 (2023-10-24)

### Feat

- **#7**: add release changelog job

## v0.7.2 (2023-10-23)

### Fix

- check for non-null PLUGINS for pages

## v0.7.1 (2023-10-18)

### Technical

- use toml commitizen configuration
- never run brakeman by default
- pin template versions

## v0.7.0 (2023-10-16)

### Feat

- **#10**: add nodejsscan job

## v0.6.2 (2023-10-06)

### Fix

- **#8**: make commitizen recognize renovate chores as bump-worthy

## v0.6.1 (2023-10-02)

### Fix

- pin twyn version

## v0.6.0 (2023-09-18)

### Feat

- add Twyn

## v0.5.1 (2023-08-23)

### Fix

- **#6**: make renovate identify all dependencies

## v0.5.0 (2023-07-18)

### Feat

- add `.do-validate-docs` to check whether docs can be built in MR pipelines

## v0.4.1 (2023-06-30)

### Fix

- make releases happen on public repository

## v0.4.0 (2023-06-23)

### Feat

- add release for every bump in the library

## v0.3.0 (2023-06-20)

### Feat

- **#1/#3**: Bump version on each commit, and publish docs in public repository
- **#2**: add renovate
- add CODEOWNERS
- pin poetry version for python's `.do-dependency-check`

### Fix

- **#1**: add commitizen configuration for bump jobs
- syntax error in python jobs
- bump job tag_prefix variable

## v0.2.4 (2023-05-31)

### Fix

- extend from local ci files

### Refactor

- push directly to remote url

## v0.2.3 (2023-05-02)

### Fix

- determine-tag now uses cz docker correctly

## v0.2.2 (2023-05-02)

### Fix

- fix gitlab-ci docker image entrypoint
- new cz docker image has `cz` as entrypoint

## v0.2.1 (2023-04-27)

### Feat

- add optional plugins for do-docs job
- add do-spell-check job

### Fix

- change spell-check to use slim instead of alpine to have bash
- attempt force git remote add

## v0.1.0 (2023-04-25)

### Feat

- add arguments for dependency scan job

## v0.0.1 (2023-04-24)
